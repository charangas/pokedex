# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
use Mix.Config

# Configure Mix tasks and generators
config :pokedex,
  ecto_repos: [Pokedex.Repo]

config :pokedex_web,
  ecto_repos: [Pokedex.Repo],
  generators: [context_app: :pokedex]

# Configures the endpoint
config :pokedex_web, PokedexWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "feQEuH37M+FabhIx23Rg//UgWKdv7d/yhDC2cI16Elf39NJ5spGh/vIMpk1KlxqD",
  render_errors: [view: PokedexWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Pokedex.PubSub,
  live_view: [signing_salt: "8P6KB6BB"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :pokedex, :poke_api,
  url: "https://pokeapi.co/api/v2",
  graphql: "https://beta.pokeapi.co/graphql/v1beta"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
