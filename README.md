# Pokedex.Umbrella

Frontend for Pokedex web app

## Getting Started

Running on your local machine for development and testing purposes.

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create the storage `mix ecto.create`
  * Runs the pending migrations `mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && yarn`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000/pokemons`](http://localhost:4000/pokemons) from your browser.

### Prerequisites

What things you need to install the Project

```
Erlang >= 22.3
Elixir >= 1.10.2
Phoenix 1.4.16
```

## Running the tests

`mix test`

### Coding style tests

`mix credo`

## Deployment

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Built With

* [Phoenix](http://www.phoenixframework.org/) - The web framework used
* [npm](https://www.npmjs.com/) - Dependency Management
* [webpack](https://webpack.js.org/) - Used to bundle assets

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](#).

## Authors

See the list of [contributors](#) who participated in this project.

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
