defmodule Pokedex.PokemonsTest do
  use Pokedex.DataCase

  alias Pokedex.Pokemons

  describe "pokemons" do
    alias Pokedex.Pokemons.Pokemon

    @valid_attrs %{abilities: "some abilities", favorite: true, height: 42, id: 42, name: "some name", number_of_skills: 42, weight: 42}
    @update_attrs %{abilities: "some updated abilities", favorite: false, height: 43, id: 43, name: "some updated name", number_of_skills: 43, weight: 43}
    @invalid_attrs %{abilities: nil, favorite: nil, height: nil, id: nil, name: nil, number_of_skills: nil, weight: nil}

    def pokemon_fixture(attrs \\ %{}) do
      {:ok, pokemon} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Pokemons.create_pokemon()

      pokemon
    end

    test "get_pokemon!/1 returns the pokemon with given id" do
      pokemon = pokemon_fixture()
      assert Pokemons.get_pokemon!(pokemon.id) == pokemon
    end

    test "create_pokemon/1 with valid data creates a pokemon" do
      assert {:ok, %Pokemon{} = pokemon} = Pokemons.create_pokemon(@valid_attrs)
      assert pokemon.abilities == "some abilities"
      assert pokemon.favorite == true
      assert pokemon.height == 42
      assert pokemon.id == 42
      assert pokemon.name == "some name"
      assert pokemon.number_of_skills == 42
      assert pokemon.weight == 42
    end

    test "create_pokemon/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Pokemons.create_pokemon(@invalid_attrs)
    end

    test "update_pokemon/2 with valid data updates the pokemon" do
      pokemon = pokemon_fixture()
      assert {:ok, %Pokemon{} = pokemon} = Pokemons.update_pokemon(pokemon, @update_attrs)
      assert pokemon.abilities == "some updated abilities"
      assert pokemon.favorite == false
      assert pokemon.height == 43
      assert pokemon.id == 43
      assert pokemon.name == "some updated name"
      assert pokemon.number_of_skills == 43
      assert pokemon.weight == 43
    end


    test "delete_pokemon/1 deletes the pokemon" do
      pokemon = pokemon_fixture()
      assert {:ok, %Pokemon{}} = Pokemons.delete_pokemon(pokemon)
      assert_raise Ecto.NoResultsError, fn -> Pokemons.get_pokemon!(pokemon.id) end
    end

    test "change_pokemon/1 returns a pokemon changeset" do
      pokemon = pokemon_fixture()
      assert %Ecto.Changeset{} = Pokemons.change_pokemon(pokemon)
    end
  end
end
