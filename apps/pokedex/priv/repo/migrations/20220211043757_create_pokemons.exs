defmodule Pokedex.Repo.Migrations.CreatePokemons do
  use Ecto.Migration

  def change do
    create table(:pokemons, primary_key: false) do

      add :id,                  :bigint
      add :name,                :string
      add :abilities,           :string
      add :height,              :integer
      add :weight,              :integer
      add :number_of_skills,    :integer
      add :favorite,            :boolean, default: true, null: false

      timestamps()
    end

    create(unique_index(:pokemons, [:id], name: :pokemons_id))
    create index(:pokemons, [:name])
    create index(:pokemons, [:height])
    create index(:pokemons, [:weight])
    create index(:pokemons, [:number_of_skills])
  end
end
