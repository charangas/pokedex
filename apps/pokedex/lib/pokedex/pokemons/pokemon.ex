defmodule Pokedex.Pokemons.Pokemon do
  @moduledoc """
    Module are implemented functions to ABC table pokemons
  """

  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]
  import Pokedex.Gettext

  alias Pokedex.Pokemons.Pokemon
  alias Pokedex.Repo

  @required_fields [:id, :favorite, :height, :name, :weight]
  @optional_fields [:abilities, :number_of_skills]
  @primary_key false

  schema "pokemons" do
    field :id,                  :integer, primary_key: true
    field :abilities,           :string
    field :favorite,            :boolean, default: true
    field :height,              :integer
    field :name,                :string
    field :number_of_skills,    :integer
    field :weight,              :integer

    timestamps()
  end

  @doc false
  def create_changeset(record, attrs) do
    record
    |> update_changeset(attrs)
    |> validate_required(@required_fields)
  end

  @doc false
  def update_changeset(record, attrs) do
    record
    |> cast(attrs, @required_fields)
    |> cast(attrs, @optional_fields)
    |> unique_constraint(:id, name: :pokemons_id, message: gettext("the pokemon id is already exist"))
  end

  def get_all() do
    query = from p in Pokemon,
            order_by: [asc: :id],
            select: %{id: p.id,
              abilities: p.abilities,
              favorite: p.favorite,
              height: p.height,
              name: p.name,
              number_of_skills: p.number_of_skills,
              weight: p.weight }

    Repo.all(query)

  end

end
