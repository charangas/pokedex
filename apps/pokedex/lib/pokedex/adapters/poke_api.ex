defmodule Pokedex.Adapters.PokeApi do
  @moduledoc """
  Module for p mails
  """

  require Logger

  def get_pokemons(:api, params) do
    offset = get_offset(params)

    case get_list_pokemon(offset, params["page_size"]) do
      {:ok, items} ->
        items
        |> Enum.reduce([], fn item, list_tasks ->
          [Task.async(fn -> send_get_request(item["url"]) end) | list_tasks]
        end)
        |> Task.await_many()
        |> Enum.map(& parse_result(&1))
      _else ->
        []
    end
  end

  def get_pokemons(:graphql, _params) do
    url = get_graphql_url()

    body = %{
      operationName: "getItems",
      variables: %{},
      query: "query getItems{pokemon_v2_pokemon{id, name, height, weight, abilities: pokemon_v2_pokemonabilities{is_hidden, ability: pokemon_v2_ability{name}}}}",
    }

    case send_post_request(url, body) do
      {:ok, %{"data" => %{"pokemon_v2_pokemon" => items}}} ->
        Logger.info "Items Count: #{Enum.count(items)}"

        Enum.map(items, & parse_result(&1))
      _else ->
        []
    end
  end

  def get_pokemon(id) do
    url = get_url() <> "/pokemon/#{id}"
    case send_get_request(url) do
      {:ok, result} ->
        parse_result(result)
      {:error, error} ->
        %{}
    end
  end

  def get_list_pokemon(offset, limit) do
    url = get_url() <> "/pokemon?offset=#{offset}&limit=#{limit}"

    case send_get_request(url) do
      {:ok, %{"results" => results}} ->
        {:ok, results}
      {:error, error} ->
        {:error, error}
    end
  end

  defp get_offset(params) do
    if params["page"] != nil && params["page_size"] != nil, do: String.to_integer(params["page"]) * String.to_integer(params["page_size"]), else: 0
  end

  defp parse_result({:error, _error}) do
    %{}
  end

  defp parse_result({:ok, data}) do
    parse_result(data)
  end

  defp parse_result(data) do
    %{
      id: data["id"],
      name: data["name"],
      height: data["height"],
      weight: data["weight"],
      number_of_skills: Enum.count(data["abilities"]),
      abilities: get_active_habilities(data["abilities"]),
      favorite: :false
    }
  end

  defp get_active_habilities(abilities) do
    abilities
    |> Enum.filter(fn ability -> ability["is_hidden"] == :true end)
    |> Enum.map(& &1["ability"]["name"])
    |> Enum.join(", ")
  end

  defp send_get_request(url) do
    Logger.info "PokeApi Request #{url}"

    url
    |> HTTPotion.get([hackney: [:insecure]])
    |> parse_response()
  end

  defp send_post_request(url, body) do
    Logger.info "PokeApi Request: #{url}"
    Logger.info "PokeApi body: #{inspect body}"

    response = HTTPotion.post url,
                              [body: Poison.encode!(body),
                                headers: [
                                  "Content-Type": "application/json",
                                  "Accept": "application/json"
                                ],
                                timeout: 15_000
                              ]

    parse_response(response)
  end

  defp parse_response(response) do
    case response do
      %HTTPotion.ErrorResponse{} ->
        {:error, response.message}
      _else ->
        case response.status_code do
          200 ->
            #Logger.info "PokeApi response #{response.body}"
            {:ok, Poison.decode!(response.body)}
          _else ->
            Logger.error "PokeApi response #{response.body}"

            response.body
            |> Poison.decode!
            |> (&{:error, &1}).()
        end
    end
  end

  defp get_url() do
    config = Application.get_env(:pokedex, :poke_api)
    config[:url]
  end

  defp get_graphql_url() do
    config = Application.get_env(:pokedex, :poke_api)
    config[:graphql]
  end

end