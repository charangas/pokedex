defmodule PokedexWeb.LiveHelpers do
  @moduledoc """
  Renders a component inside the `PokedexWeb.ModalComponent` component.

  The rendered modal receives a `:return_to` option to properly update
  the URL when the modal is closed.

  ## Examples

      <%= live_modal @socket, PokedexWeb.PokemonLive.FormComponent,
        id: @pokemon.id || :new,
        action: @live_action,
        pokemon: @pokemon,
        return_to: Routes.pokemon_index_path(@socket, :index) %>
  """

  import Phoenix.LiveView.Helpers

  def live_modal(_socket, component, opts) do
    path = Keyword.fetch!(opts, :return_to)
    modal_opts = [id: :modal, return_to: path, component: component, opts: opts]
    live_component(PokedexWeb.ModalComponent, modal_opts)
  end
end
