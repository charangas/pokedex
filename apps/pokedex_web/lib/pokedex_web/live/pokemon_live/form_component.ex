defmodule PokedexWeb.PokemonLive.FormComponent do
  @moduledoc false

  use PokedexWeb, :live_component

  alias Pokedex.Pokemons

  @impl true
  def update(%{pokemon: pokemon} = assigns, socket) do
    changeset = Pokemons.change_pokemon(pokemon)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"pokemon" => pokemon_params}, socket) do
    changeset =
      socket.assigns.pokemon
      |> Pokemons.change_pokemon(pokemon_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"pokemon" => pokemon_params}, socket) do
    save_pokemon(socket, socket.assigns.action, pokemon_params)
  end

  defp save_pokemon(socket, :edit, pokemon_params) do
    case Pokemons.update_pokemon(socket.assigns.pokemon, pokemon_params) do
      {:ok, _pokemon} ->
        {:noreply,
         socket
         |> put_flash(:info, "Pokemon updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_pokemon(socket, :new, pokemon_params) do
    case Pokemons.create_pokemon(pokemon_params) do
      {:ok, _pokemon} ->
        {:noreply,
         socket
         |> put_flash(:info, "Pokemon created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
