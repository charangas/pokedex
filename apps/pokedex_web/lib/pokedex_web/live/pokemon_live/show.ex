defmodule PokedexWeb.PokemonLive.Show do
  @moduledoc false

  use PokedexWeb, :live_view

  alias Pokedex.Pokemons

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _default, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:pokemon, Pokemons.get_pokemon!(id))}
  end

  defp page_title(:show), do: "Show Pokemon"
  defp page_title(:edit), do: "Edit Pokemon"
end
