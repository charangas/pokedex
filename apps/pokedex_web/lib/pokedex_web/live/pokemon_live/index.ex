defmodule PokedexWeb.PokemonLive.Index do
  @moduledoc false

  use PokedexWeb, :live_view

  alias Pokedex.Pokemons
  alias Pokedex.Pokemons.Pokemon
  alias PokedexWeb.Router.Helpers, as: Routes

  @impl true
  def mount(params, _session, socket) do
    page =
      Pokemons.list_pokemons(params)

    %{entries: entries, page_number: page_number, page_size: page_size, total_entries: total_entries, total_pages: total_pages} = page

    assigns = [
      conn: socket,
      pokemons: entries,
      page: page,
      page_number: page_number || 0,
      page_size: page_size || 0,
      total_entries: total_entries || 0,
      total_pages: total_pages || 0
    ]

    {:ok, assign(socket, assigns)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Pokemon")
    |> assign(:pokemon, Pokemons.get_pokemon!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Pokemon")
    |> assign(:pokemon, %Pokemon{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Pokemons")
    |> assign(:pokemon, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    pokemon = Pokemons.get_pokemon!(id)
    {:ok, _} = Pokemons.delete_pokemon(pokemon)

    {:noreply, assign(socket, :pokemons, Pokemons.list_pokemons())}
  end

  def handle_event("nav", %{"page" => page, "page_size" => page_size}, socket) do
    {:noreply, push_redirect(socket, to: Routes.pokemon_index_path(socket, socket.assigns.live_action, [page: page, page_size: page_size]))}
  end

  def handle_event("change-page-size", %{"page_size" => page_size}, socket) do
    {:noreply, push_redirect(socket, to: Routes.pokemon_index_path(socket, :index, [page_size: page_size]))}
  end

  def handle_event("favorite", params, socket) do
    record = Poison.decode!(params["ref"])

    if params["value"] == "on" do
      Pokemons.create_pokemon(%{record | "favorite" => true})
    else
      record["id"]
      |> Pokemons.get_pokemon!()
      |> Pokemons.delete_pokemon()
    end

    {:noreply, socket}
  end

end
