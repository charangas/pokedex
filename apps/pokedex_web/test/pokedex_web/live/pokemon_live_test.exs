defmodule PokedexWeb.PokemonLiveTest do
  use PokedexWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Pokedex.Pokemons

  @create_attrs %{abilities: "some abilities", favorite: true, height: 42, id: 42, name: "some name", number_of_skills: 42, weight: 42}
  @update_attrs %{abilities: "some updated abilities", favorite: false, height: 43, id: 43, name: "some updated name", number_of_skills: 43, weight: 43}
  @invalid_attrs %{abilities: nil, favorite: nil, height: nil, id: nil, name: nil, number_of_skills: nil, weight: nil}

  defp fixture(:pokemon) do
    {:ok, pokemon} = Pokemons.create_pokemon(@create_attrs)
    pokemon
  end

  defp create_pokemon(_default) do
    pokemon = fixture(:pokemon)
    %{pokemon: pokemon}
  end

  describe "Index" do
    setup [:create_pokemon]

    test "lists all pokemons", %{conn: conn, pokemon: pokemon} do
      {:ok, _index_live, html} = live(conn, Routes.pokemon_index_path(conn, :index))

      assert html =~ "Listing Pokemons"
      assert html =~ pokemon.abilities
    end

  end

  describe "Show" do
    setup [:create_pokemon]

    test "displays pokemon", %{conn: conn, pokemon: pokemon} do
      {:ok, _show_live, html} = live(conn, Routes.pokemon_show_path(conn, :show, pokemon))

      assert html =~ "Show Pokemon"
      assert html =~ pokemon.abilities

    end

  end
end
